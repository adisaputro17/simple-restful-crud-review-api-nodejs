# Simple RESTful CRUD Review API NodeJS

## Installation

1. Clone this repository

        git clone https://gitlab.com/adisaputro17/tugas-crud-nodejs.git

2. Install dependencies

        npm install

3. Open `create_db.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: ''

4. Run `create_db.js`

        node create_db.js

5. Open `create_table.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: '',
        database: 'db_nodejs',

6. Run `create_table.js`

        node create_table.js

7. Open `index.js` and modify connection variable values

        host: 'localhost',
        user: 'root',
        password: '',
        database: 'db_nodejs',

8. Run `index.js`

        node index.js

## API Documentation

1. Create new review

    ### Request
        POST    /api/review
    
    ### Request form-data
        nama    :   string
        rating  :   int
        ulasan  :   string
        file    :   file image

2. Get all review
    ### Request
        GET     /api/reviews

3. Get detail review with id
    ### Request
        GET     /api/review/{id}

4. Update existing review
    ### Request
        PUT     /api/review/{id}

    ### Request form-data
        nama    :   string
        rating  :   int
        ulasan  :   string

5. Delete review
    ### Request
        DELETE      /delete/{id}
