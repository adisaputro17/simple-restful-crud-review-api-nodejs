var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: "review_db",
});

//Membuka koneksi ke database MySQL
connection.connect(function(err){
    if(err) {
        console.log(err);
    }
});

// Query bisa dilakukan di sini
var sql = `CREATE TABLE review
(
    id int(11) NOT NULL AUTO_INCREMENT,
    nama varchar(50) NOT NULL,
    rating int(1) NOT NULL,
    ulasan text,
    image varchar(50),
    PRIMARY KEY (id)
)`;

connection.query(sql, function(err){
    if(err){
      console.log(err);
    } else {
      console.log("Table created");
    }
});

//Menutup koneksi
connection.end(function(err){
    if(err) {
        console.log(err);
     }
});