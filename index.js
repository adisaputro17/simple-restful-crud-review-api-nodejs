const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');

const multer  = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, './images');
    },
    filename: (req, file, cb) => {
      console.log(file);
      var filetype = '';
      if(file.mimetype === 'image/gif') {
        filetype = 'gif';
      }
      if(file.mimetype === 'image/png') {
        filetype = 'png';
      }
      if(file.mimetype === 'image/jpeg') {
        filetype = 'jpg';
      }
      cb(null, 'image-' + Date.now() + '.' + filetype);
    }
});
var upload = multer({storage: storage});

// parse application/json
app.use(bodyParser.json());

//create database connection
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'review_db'
});

//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});

//menampilkan semua review
app.get('/api/reviews',(req, res) => {
  let sql = "SELECT * FROM review";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.json({"status": 200, "message": "success", "response": results})
  });
});

//menampilkan data review berdasarkan id
app.get('/api/review/:id',(req, res) => {
  let sql = "SELECT * FROM review WHERE id="+req.params.id;
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.json({"status": 200, "message": "success", "response": results});
  });
});

//menambahkan review
app.post('/api/review',upload.single('file'),function(req, res, next) {
    console.log(req.file);
    if(!req.file) {
      res.status(500);
      return next(err);
    }
    let data = {nama: req.body.nama, rating: req.body.rating, ulasan: req.body.ulasan, image: req.file.filename,};
    let sql = "INSERT INTO review SET ?";
    let query = conn.query(sql, data,(err, results) => {
        if(err) throw err;
        res.json({"status": 200, "message": "review successfully added"});
    });
});

//mengedit review berdasarkan id
app.put('/api/review/:id',(req, res) => {
  let sql = "UPDATE review SET nama='"+req.body.nama+"', rating='"+req.body.rating+"', ulasan='"+req.body.ulasan+"' WHERE id="+req.params.id;
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.json({"status": 200, "message": "review successfully updated"});
  });
});
 
//menghapus review berdasarkan id
app.delete('/api/review/:id',(req, res) => {
  let sql = "DELETE FROM review WHERE id="+req.params.id+"";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
      res.json({"status": 200, "message": "review deleted"});
  });
});

//Server listening
app.listen(3000,() =>{
  console.log('Server started on port 3000...');
});